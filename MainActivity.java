package com.finalprogect.game;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

/*
*  android:screenOrientation="landscape" ->把銀幕調成橫向
*  <style name="AppTheme" parent="Theme.AppCompat.Light.NoActionBar"> ->設定全銀幕+上面沒有電池時間等
        <item name="android:windowNoTitle">true</item>
        <item name="android:windowActionBar">false</item>
        <item name="android:windowFullscreen">true</item>
        <item name="android:windowContentOverlay">@null</item>
   </style>
* */

public class MainActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    //Button開頭的function都是在Xml中相對的ImageButton呼叫 所以不用宣告
     public void buttonGameMenuClick(View v)
     {
         Intent intent = new Intent(MainActivity.this, GameMenuActivity.class);
         startActivity(intent);
     }

    public void buttonDataClick(View v)
    {
        Intent intent = new Intent(MainActivity.this, DataActivity.class);
        startActivity(intent);
    }

    public void buttonInformaionClick(View v)
    {
        Intent intent = new Intent(MainActivity.this, InformationActivity.class);
        startActivity(intent);
    }

    public void buttonExitClick(View v)
    {
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);
    }
}

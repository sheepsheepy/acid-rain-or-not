package com.finalprogect.game;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class InformationActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);
    }

    public void ReturnMain(View v)
    {
        InformationActivity.this.finish();
    }
}

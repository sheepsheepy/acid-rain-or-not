package com.finalprogect.game;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.finalprogect.game.LocationUtil.PermissionUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.StringTokenizer;


public class GameMenuActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,ActivityCompat.OnRequestPermissionsResultCallback,
        PermissionUtils.PermissionResultCallback
{
    String[] LocationArray = {"Saddle", "Taipei", "Zhongli", "Hsinchu", "Taichung", "Nantou", "Yunlin", "Chiayi", "Tainan", "Kaohsiung", "Hengchuen", "Yilan", "Chenggong"};
    String[] LocationNameArray = {"臺北", "新北", "桃園", "新竹", "臺中", "南投", "雲林", "嘉義", "臺南", "高雄", "屏東", "宜蘭", "台東"};
    double[][] data =   {{25000,25000,25000,20000,10000,25000,20000,15000,25000,15000,20000,20000,20000}, //遊戲長度
                        {4.8,4.8,6,3.6,1.2,3,2.4,1.2,2.4,1.2,1.8,3.6,2.4}, //酸雨傷害
                        {50,40,50,50,30,30,50,30,30,40,30,40,40}, //酸雨數量
                        {2,1,1,3,5,1,4,4,2,4,3,2,3}, //雨傘數量
                        {4,4,5,3,1,5,2,2,4,1,3,3,2}, //車子數量
                        {60,45,65,55,5,25,50,10,20,30,15,40,35}, //一元數量
                        {6,6,6,6,2,2,6,2,2,4,2,4,4}, //十元數量
                        {3,2,3,3,1,1,3,1,1,2,1,2,2}}; //50元數量
    int[] checkopened =  {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    int[] checkplayed  = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    int[] achieve  = {0, 0, 0, 0, 0, 0, 0};
    int locationchecknumber = 0, checknum = 0, money = 0;
    ConstraintLayout LevelLayout;
    ImageButton Locationbtn;
    Button word;
    ImageButton Check, Cencel;
    ImageView levelimage, raining;
    TextView walletmoney, level, wordbackground;
    Activity activity = this;

    //GPS
    private static final String TAG = MyLocationUsingHelper.class.getSimpleName();
    private final static int PLAY_SERVICES_REQUEST = 1000;
    private final static int REQUEST_CHECK_SETTINGS = 2000;

    ImageButton gps;
    Location myLocation;

    private GoogleApiClient myApiClient;

    double latitude;
    double longitude;

    ArrayList<String> permissions = new ArrayList<>();
    PermissionUtils permissionUtils;

    boolean isPermissionGranted;

    Handler handler;
    RainDataParser rainDataParser;
    String dataStr;
    boolean isDataReady;
    HashMap<String, Float> dataMap;
    boolean isRain;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gamemenu);
        findViews();
        ReadData();
        setInvisable();
        locationClickListeners();
        gps = (ImageButton)findViewById(R.id.gps);
        permissionUtils = new PermissionUtils(GameMenuActivity.this);

        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);

        permissionUtils.check_permission(permissions,"Need GPS permission for getting your location",1);
        if (checkPlayServices())
            buildGoogleApiClient();

        isDataReady = false;
        handler = new Handler();
        dataStr = new String("");
        dataMap = null;
        isRain = false;

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while(dataMap == null){
                    rainDataParser = new RainDataParser();

                    while(!rainDataParser.getXML()){}

                    while(!rainDataParser.parseXml()){}
                    isDataReady = true;
                    dataMap = rainDataParser.getRain();
                }
            }
        });
        thread.start();
    }
    public void GPSClick(View v)
    {
        getLocation();
        if (myLocation != null) {
            latitude = myLocation.getLatitude();
            longitude = myLocation.getLongitude();
            getAddress();
        }
    }
    protected void findViews()
    {
        walletmoney = (TextView)findViewById(R.id.money);
        word = (Button)findViewById(R.id.word);
        Check = (ImageButton)findViewById(R.id.green);
        Cencel = (ImageButton)findViewById(R.id.red);
        wordbackground = (TextView)findViewById(R.id.wordbackground);
        LevelLayout = (ConstraintLayout)findViewById(R.id.LevelLayout);
        level = (TextView)findViewById(R.id.level);
        levelimage = (ImageView)findViewById(R.id.levelimage);
        raining = (ImageView)findViewById(R.id.raining);
    }

    public void SaveData()
    {
        SharedPreferences sharedPreferences = getSharedPreferences("data", MODE_PRIVATE);
        StringBuilder openedstring = new StringBuilder();
        for(int i = 0; i < 13; i++)
            openedstring.append(checkopened[i]).append(",");
        StringBuilder playedstring = new StringBuilder();
        for(int i = 0; i < 13; i++)
            playedstring.append(checkplayed[i]).append(",");

        StringBuilder achievestring = new StringBuilder();
        for(int i = 0; i < 7; i++)
            achievestring.append(achieve[i]).append(",");

        sharedPreferences.edit().putInt("money", money).putString("opened",openedstring.toString())
                .putString("played", playedstring.toString())
                .putString("achieve",achievestring.toString())
                .commit();
    }
    public void ReadData()
    {
        SharedPreferences sharedPreferences = getSharedPreferences("data", MODE_PRIVATE);
        //getmoney
        money = sharedPreferences.getInt("money",0);
        //getopened
        String opentaken = sharedPreferences.getString("opened","0,0,0,0,0,0,0,0,0,0,0,0,0");
        StringTokenizer openedstring = new StringTokenizer(opentaken, ",");
        for(int i=0;i<checkopened.length;i++)
            checkopened[i] = Integer.parseInt(openedstring.nextToken());
        //getplayed
        String playtaken = sharedPreferences.getString("played","0,0,0,0,0,0,0,0,0,0,0,0,0");
        StringTokenizer playedstring = new StringTokenizer(playtaken, ",");
        for(int i=0;i<checkplayed.length;i++)
            checkplayed[i] = Integer.parseInt(playedstring.nextToken());

        String achievetaken = sharedPreferences.getString("achieve","0,0,0,0,0,0,0");
        StringTokenizer achievestring = new StringTokenizer(achievetaken, ",");
        for(int i=0;i<achieve.length;i++)
            achieve[i] = Integer.parseInt(achievestring.nextToken());
        updataMoneyAndMap();
        checkmanymoney();
        checknomoney();
    }
    protected void updataMoneyAndMap()
    {
        walletmoney.setText("$" + String.valueOf(money));
        for(int i = 0; i<13; i++)
        {
            if(checkopened[i] == 1)
            {
                int locationInt = getResources().getIdentifier(LocationArray[i], "id", getPackageName());
                Locationbtn = (ImageButton) findViewById(locationInt);
                Locationbtn.setImageResource(getResources().getIdentifier("ic_openlocation","drawable",getPackageName()));
            }
        }
    }

    protected void locationClickListeners()
    {
        for(int i=0;i<13;i++)
        {
            final int place = i;
            int locationInt = getResources().getIdentifier(LocationArray[i], "id", getPackageName());
            Locationbtn = (ImageButton)findViewById(locationInt);
            Locationbtn.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    String locationName = LocationNameArray[place];

                    isRain = false;
                    if(dataMap != null){
                        for (Map.Entry<String, Float> entry: dataMap.entrySet()) {
                            if(entry.getKey().contains(locationName)){
                                isRain = true;
                                break;
                            }
                        }
                    }
                    showraining(isRain);
                    if (checkopened[place] == 1)
                        word.setText("現在點選的是「" + locationName + "」\n要前往嗎?");
                    else
                        word.setText("要買"+locationName+"的車票嗎?");
                    locationchecknumber = place;
                    word.setClickable(false);
                    setVisable();
                    if (checkplayed[place] == 1)
                    {
                        switch ((int) data[0][place] * 2) {
                            case 20000:
                                levelimage.setImageResource(getResources().getIdentifier("ic_rain_one", "drawable", getPackageName()));
                                break;
                            case 30000:
                                levelimage.setImageResource(getResources().getIdentifier("ic_rain_two", "drawable", getPackageName()));
                                break;
                            case 40000:
                                levelimage.setImageResource(getResources().getIdentifier("ic_rain_three", "drawable", getPackageName()));
                                break;
                            case 50000:
                                levelimage.setImageResource(getResources().getIdentifier("ic_rain_four", "drawable", getPackageName()));
                                break;
                        }
                    }
                    else
                        levelimage.setImageResource(getResources().getIdentifier("ic_ques", "drawable", getPackageName()));

                }
            });
        }
    }

    public void Checkclick(View v)
    {
        setInvisable();
        WordClick(v);
        word.setClickable(true);

        if(checkopened[locationchecknumber] == 1)
        {
            checkplayed[locationchecknumber] = 1;
            SaveData();
            passBundle();
        }
        else
        {
            if(money - 590 < 0 )
                word.setText("錢太少了喔!!");
            else
            {
                money = money - 590;
                checkopened[locationchecknumber] = 1;
                checknomoney();
                addall(1);
                updataMoneyAndMap();
            }
        }
    }
    public void Cencelclick(View v)
    {
        WordClick(v);
        setInvisable();
        word.setClickable(true);
    }
    public void passBundle()
    {
        //Toast.makeText(this, String.valueOf(data[0][locationchecknumber]), Toast.LENGTH_SHORT).show();
        //Toast.makeText(this, String.valueOf(data[8][locationchecknumber]), Toast.LENGTH_SHORT).show();
        Bundle bundle = new Bundle();
        String locationName = LocationNameArray[locationchecknumber];

        bundle.putString("Location", LocationNameArray[locationchecknumber]);
        bundle.putInt("Long", (int)data[0][locationchecknumber]);
        bundle.putDouble("RainHurt", data[1][locationchecknumber]);
        bundle.putInt("RainNumber", (int)data[2][locationchecknumber]);
        bundle.putInt("UmbrellaNumber", (int)data[3][locationchecknumber]);
        bundle.putInt("CarNumber", (int)data[4][locationchecknumber]);
        bundle.putInt("OneDollar", (int)data[5][locationchecknumber]);
        bundle.putInt("TenDollar", (int)data[6][locationchecknumber]);
        bundle.putInt("FiftyDollar", (int)data[7][locationchecknumber]);
        bundle.putBoolean("isRain", isRain);

        Intent intent = new Intent(GameMenuActivity.this, AndroidLauncher.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void getPassBundle(){
        Bundle bundle = getIntent().getExtras();
        if(bundle != null )
        {
            addMoney(bundle.getInt("money"));
            addcar(bundle.getInt("car"));
            addrain(bundle.getInt("rain"));
            addumbrella(bundle.getInt("umbrella"));
            addwin(bundle.getInt("win"));
        }
    }

    public void WordClick(View v)
    {
        Random random = new Random();
        switch (checkchange((random.nextInt(100)+1) % 8))
        {
            case 0:
                break;
            case 1:
                WordClick(v);
                break;
        }
    }
    protected int checkchange(int n)
    {
        if(n != checknum)
        {
            checknum = n;
            switch (n)
            {
                case 0:
                    word.setText("到底哪裡的雨是最酸的呢?");
                    break;
                case 1:
                    word.setText("惡有惡報嗎?");
                    break;
                case 2:
                    word.setText("如果要前往其他地方的話就要買車票喔!!");
                    break;
                case 3:
                    word.setText("不要一直淋雨會感冒喔!");
                    break;
                case 4:
                    word.setText("酸度跟雨量成正比嗎?");
                    break;
                case 5:
                    word.setText("雨很大呢~記得撐傘~");
                    break;
                case 6:
                    word.setText("玩完所有關卡會不會有什麼東西呢?");
                    break;
                case 7:
                    word.setText("有空去看一看酸雨資料吧!");
                    break;
            }
            return 0;
        }
        else
            return 1;
    }

    protected void setInvisable()
    {
        Check.setVisibility(View.INVISIBLE);
        Cencel.setVisibility(View.INVISIBLE);
        wordbackground.setVisibility(View.INVISIBLE);
        LevelLayout.setVisibility(View.INVISIBLE);
        level.setVisibility(View.INVISIBLE);
        levelimage.setVisibility(View.INVISIBLE);
    }
    protected void setVisable()
    {
        Check.setVisibility(View.VISIBLE);
        Cencel.setVisibility(View.VISIBLE);
        wordbackground.setVisibility(View.VISIBLE);
        LevelLayout.setVisibility(View.VISIBLE);
        level.setVisibility(View.VISIBLE);
        levelimage.setVisibility(View.VISIBLE);
    }

    public void CGClick(View v)
    {
        Intent intent = new Intent(GameMenuActivity.this, CGActivity.class);
        Bundle bundle = new Bundle();
        bundle.putIntArray("cgplayed",checkplayed);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void CrownClick(View v)
    {
        Intent intent = new Intent(GameMenuActivity.this, AchieveActivity.class);
        Bundle bundle = new Bundle();
        bundle.putIntArray("achieve",achieve);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void showraining(boolean n)
    {
        if(!n)
            raining.setVisibility(View.INVISIBLE);
        else
            raining.setVisibility(View.VISIBLE);
    }

    public void addMoney(int n)
    {
        money = money + n;
        walletmoney.setText("$" + String.valueOf(money));
    }

    public void addcar(int n)
    {
        achieve[0] = achieve[0] + n;
    }
    public void addrain(int n)
    {
        achieve[1] = achieve[1] + n;
    }
    public void addumbrella(int n)
    {
        achieve[2] = achieve[2] + n;
    }
    public void checkmanymoney()
    {
        if(money >= 30000)
            achieve[3] = 1;
    }
    public void checknomoney()
    {
        if(money <= 10)
            achieve[4] = 1;
    }
    public void addwin(int n)
    {
        achieve[5] = achieve[5] + n;
    }
    public void addall(int n)
    {
        achieve[6] = achieve[6] + n;
    }


    private void getLocation() {

        if (isPermissionGranted)
        {
            try
            {
                myLocation = LocationServices.FusedLocationApi.getLastLocation(myApiClient);
            }
            catch (SecurityException e)
            {
                e.printStackTrace();
            }

        }

    }

    public Address getAddress(double latitude, double longitude)
    {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude,longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            return addresses.get(0);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;

    }

    public void getAddress()
    {

        Address locationAddress=getAddress(latitude,longitude);
        int locationInt;
        if(locationAddress!=null)
        {
            String postalCode = locationAddress.getPostalCode();
            int postnumber = Integer.valueOf(postalCode);
            //"鞍部", "臺北", "中壢", "新竹", "臺中", "南投", "雲林", "嘉義", "臺南", "高雄", "屏東", "宜蘭", "台東"
            if((postnumber >= 100 && postnumber <=116)||(postnumber >= 200  && postnumber <= 206))//台北
            {
                locationInt = getResources().getIdentifier(LocationArray[1], "id", getPackageName());
                checkopened[1] = 1;
            }
            else if (postnumber >= 320 && postnumber <= 338)
            {
                locationInt = getResources().getIdentifier(LocationArray[2], "id", getPackageName());
                checkopened[2] = 1;
            }
            else if(postnumber >= 300 && postnumber <= 315)
            {
                locationInt = getResources().getIdentifier(LocationArray[3], "id", getPackageName());
                checkopened[3] = 1;
            }
            else if(postnumber >= 400 && postnumber <= 439)
            {
                locationInt = getResources().getIdentifier(LocationArray[4], "id", getPackageName());
                checkopened[4] = 1;
            }
            else if(postnumber >= 540 && postnumber <= 558)
            {
                locationInt = getResources().getIdentifier(LocationArray[5], "id", getPackageName());
                checkopened[5] = 1;
            }
            else if(postnumber >= 630 && postnumber <= 655)
            {
                locationInt = getResources().getIdentifier(LocationArray[6], "id", getPackageName());
                checkopened[6] = 1;
            }
            else if(postnumber >= 600 && postnumber <= 625)
            {
                locationInt = getResources().getIdentifier(LocationArray[7], "id", getPackageName());
                checkopened[7] = 1;
            }
            else if(postnumber >= 700 && postnumber <= 745)
            {
                locationInt = getResources().getIdentifier(LocationArray[8], "id", getPackageName());
                checkopened[8] = 1;
            }
            else if(postnumber >= 800 && postnumber <= 852)
            {
                locationInt = getResources().getIdentifier(LocationArray[9], "id", getPackageName());
                checkopened[9] = 1;
            }
            else if(postnumber >= 900 && postnumber <= 947)
            {
                locationInt = getResources().getIdentifier(LocationArray[10], "id", getPackageName());
                checkopened[10] = 1;
            }
            else if(postnumber >= 260 && postnumber <= 290)
            {
                locationInt = getResources().getIdentifier(LocationArray[11], "id", getPackageName());
                checkopened[11] = 1;
            }
            else if(postnumber >= 950 && postnumber <= 966)
            {
                locationInt = getResources().getIdentifier(LocationArray[12], "id", getPackageName());
                checkopened[12] = 1;
            }
            else
            {
                locationInt = getResources().getIdentifier(LocationArray[5], "id", getPackageName());
                checkopened[5] = 1;
            }
            Locationbtn = (ImageButton) findViewById(locationInt);
            Locationbtn.setImageResource(getResources().getIdentifier("ic_nowpoint","drawable",getPackageName()));
            SaveData();
        }
    }

    protected synchronized void buildGoogleApiClient() {
        myApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();

        myApiClient.connect();

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(myApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>()
        {
            @Override
            public void onResult(LocationSettingsResult locationSettingsResult)
            {

                final Status status = locationSettingsResult.getStatus();

                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location requests here
                        getLocation();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(GameMenuActivity.this, REQUEST_CHECK_SETTINGS);

                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });
    }





    private boolean checkPlayServices() {

        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();

        int resultCode = googleApiAvailability.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(resultCode)) {
                googleApiAvailability.getErrorDialog(this,resultCode,
                        PLAY_SERVICES_REQUEST).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        // All required changes were successfully made
                        getLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        // The user was asked to change settings, but chose not to
                        break;
                    default:
                        break;
                }
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Connect the client.
        myApiClient.connect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPlayServices();
        getPassBundle();
    }

    /**
     * Google api callback methods
     */
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
    }

    @Override
    public void onConnected(Bundle arg0) {

        // Once connected with google api, get the location
        getLocation();
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        myApiClient.connect();
    }


    // Permission check functions


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        // redirects to utils
        permissionUtils.onRequestPermissionsResult(requestCode,permissions,grantResults);

    }




    @Override
    public void PermissionGranted(int request_code) {
        Log.i("PERMISSION","GRANTED");
        isPermissionGranted=true;
    }

    @Override
    public void PartialPermissionGranted(int request_code, ArrayList<String> granted_permissions) {
        Log.i("PERMISSION PARTIALLY","GRANTED");
    }

    @Override
    public void PermissionDenied(int request_code) {
        Log.i("PERMISSION","DENIED");
    }

    @Override
    public void NeverAskAgain(int request_code) {
        Log.i("PERMISSION","NEVER ASK AGAIN");
    }
    public void ReturnMain(View v)
    {
        SaveData();
        GameMenuActivity.this.finish();
    }

}

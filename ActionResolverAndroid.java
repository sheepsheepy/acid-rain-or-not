package com.finalprogect.game;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

/**
 * Created by marcia on 2017/12/22.
 */

public class ActionResolverAndroid implements ActionResolver {
    Context context;
    Handler handler;
    String locationName;
    int length;
    float rainHarm;
    int rainNumber;
    int umbrellaNumber;
    int carNumber;
    int coinNum1;
    int coinNum10;
    int coinNum50;
    int level;
    public ActionResolverAndroid(Context context){
        this.context = context;
        handler = new Handler();
    }
    @Override
    public void exitGame() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                //Toast.makeText(context, "exit game", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context, GameMenuActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public String getLocationName() {
        return locationName;
    }

    @Override
    public int getLength() {
        return length;
    }

    @Override
    public float getRainHarm() {
        return rainHarm;
    }

    @Override
    public int getRainNumber() {
        return rainNumber;
    }

    @Override
    public int getUmbrellaNumber() {
        return umbrellaNumber;
    }

    @Override
    public int getCoinNum1() {
        return coinNum1;
    }

    @Override
    public int getCoinNum10() {
        return coinNum10;
    }

    @Override
    public int getCoinNum50() {
        return coinNum50;
    }

    public int getCarNumber(){
        return carNumber;
    }

    public int getLevel(){
        return level;
    }

    @Override
    public void sentData(int money, int hitByRain, int hitByUmbrella, int hitByCar, int win) {
        Intent intent = new Intent(context, GameMenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Bundle bundle = new Bundle();
        if(bundle != null )
        {
            bundle.putInt("money", money);
            bundle.putInt("car", hitByCar);
            bundle.putInt("rain", hitByRain);
            bundle.putInt("umbrella", hitByUmbrella);
            bundle.putInt("win", win);
            intent.putExtras(bundle);

            context.startActivity(intent);
        }
    }
}

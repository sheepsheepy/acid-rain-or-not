package com.finalprogect.game;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.*;

public class CGActivity extends AppCompatActivity
{
    ImageView cgimage;
    View prebutton;
    int[] check = {0,0,0,0,0,0,0,0,0,0,0,0,0};
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cg);
        cgimage = (ImageView)findViewById(R.id.CGpng);
        prebutton = (android.widget.Button)findViewById(R.id.saddle);
        Bundle bundle = this.getIntent().getExtras();
        check = bundle.getIntArray("cgplayed");
    }
    //我先用ic_cg做測試 那邊應該要放對應的圖
    public void SaddleClick(View v)
    {
        prebutton.setBackgroundColor(Color.rgb(223,241,255));
        v.setBackgroundColor(Color.rgb(164,215,255));
        if(check[0] == 1)
            cgimage.setImageResource(getResources().getIdentifier("clear_0", "drawable", getPackageName()));
        prebutton = v;
    }
    public void TaipeiClick(View v)
    {
        prebutton.setBackgroundColor(Color.rgb(223,241,255));
        v.setBackgroundColor(Color.rgb(164,215,255));
        if(check[1] == 1)
            cgimage.setImageResource(getResources().getIdentifier("clear_1", "drawable", getPackageName()));
        prebutton = v;
    }
    public void ZhongliClick(View v)
    {
        prebutton.setBackgroundColor(Color.rgb(223,241,255));
        v.setBackgroundColor(Color.rgb(164,215,255));
        if(check[2] == 1)
            cgimage.setImageResource(getResources().getIdentifier("clear_2", "drawable", getPackageName()));
        prebutton = v;
    }
    public void HsinchuClick(View v)
    {
        prebutton.setBackgroundColor(Color.rgb(223,241,255));
        v.setBackgroundColor(Color.rgb(164,215,255));
        if(check[3] == 1)
            cgimage.setImageResource(getResources().getIdentifier("clear_3", "drawable", getPackageName()));
        prebutton = v;
    }
    public void TaichungClick(View v)
    {
        prebutton.setBackgroundColor(Color.rgb(223,241,255));
        v.setBackgroundColor(Color.rgb(164,215,255));
        if(check[4] == 1)
            cgimage.setImageResource(getResources().getIdentifier("clear_4", "drawable", getPackageName()));
        prebutton = v;
    }
    public void NantouClick(View v)
    {
        prebutton.setBackgroundColor(Color.rgb(223,241,255));
        v.setBackgroundColor(Color.rgb(164,215,255));
        if(check[5] == 1)
            cgimage.setImageResource(getResources().getIdentifier("clear_5", "drawable", getPackageName()));
        prebutton = v;
    }
    public void YunlinClick(View v)
    {
        prebutton.setBackgroundColor(Color.rgb(223,241,255));
        v.setBackgroundColor(Color.rgb(164,215,255));
        if(check[6] == 1)
            cgimage.setImageResource(getResources().getIdentifier("clear_6", "drawable", getPackageName()));
        prebutton = v;
    }
    public void ChiayiClick(View v)
    {
        prebutton.setBackgroundColor(Color.rgb(223,241,255));
        v.setBackgroundColor(Color.rgb(164,215,255));
        if(check[7] == 1)
            cgimage.setImageResource(getResources().getIdentifier("clear_7", "drawable", getPackageName()));
        prebutton = v;
    }
    public void TainanClick(View v)
    {
        prebutton.setBackgroundColor(Color.rgb(223,241,255));
        v.setBackgroundColor(Color.rgb(164,215,255));
        if(check[8] == 1)
            cgimage.setImageResource(getResources().getIdentifier("clear_8", "drawable", getPackageName()));
        prebutton = v;
    }
    public void KaohsiungClick(View v)
    {
        prebutton.setBackgroundColor(Color.rgb(223,241,255));
        v.setBackgroundColor(Color.rgb(164,215,255));
        if(check[9] == 1)
            cgimage.setImageResource(getResources().getIdentifier("clear_9", "drawable", getPackageName()));
        prebutton = v;
    }
    public void HengchuenClick(View v)
    {
        prebutton.setBackgroundColor(Color.rgb(223,241,255));
        v.setBackgroundColor(Color.rgb(164,215,255));
        if(check[10] == 1)
            cgimage.setImageResource(getResources().getIdentifier("clear_10", "drawable", getPackageName()));
        prebutton = v;
    }
    public void YilanClick(View v)
    {
        prebutton.setBackgroundColor(Color.rgb(223,241,255));
        v.setBackgroundColor(Color.rgb(164,215,255));
        if(check[11] == 1)
            cgimage.setImageResource(getResources().getIdentifier("clear_11", "drawable", getPackageName()));
        prebutton = v;
    }
    public void ChenggongClick(View v)
    {
        prebutton.setBackgroundColor(Color.rgb(223,241,255));
        v.setBackgroundColor(Color.rgb(164,215,255));
        if(check[12] == 1)
             cgimage.setImageResource(getResources().getIdentifier("clear_12", "drawable", getPackageName()));
        prebutton = v;
    }

    public void ReturnMain(View v)
    {
        CGActivity.this.finish();
    }
}

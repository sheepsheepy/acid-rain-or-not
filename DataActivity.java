package com.finalprogect.game;

import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.achartengine.util.MathHelper;

public class DataActivity extends AppCompatActivity
{
    //10 -> 基隆 11 -> 花蓮
    String[] location = {"Taipei", "Hsinchu", "Taichung", "Chiayi", "Nantou", "Tainan", "Kaohsiung", "Pingtung", "Yilan", "Taitung", "Keelung", "Hualien"};
    String[] locationName = {"臺北","新竹","臺中","嘉義","南投","臺南","高雄","屏東","宜蘭","臺東","基隆","花蓮"};
    String[] year = {"2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016"};
    int[] locationWanted = {1,0,0,0,0,0,0,0,0,0,0,0}; //儲存使用者需要哪些地方的資料
    int[] yearwanted = {1,0,0,0,0,0,0,0,0}; //儲存使用者要那些年分
    int datatype = 0; //0是酸雨資料 1是雨量資料
    int datanumber = 10; //酸雨資料的data有10筆 雨量有12筆
    double[][] raindata =
            {{139.10, 189.86, 146.55, 242.53, 211.78, 178.94, 209.93, 202.64},
            {93.29, 133.76, 101.88, 228.43, 170.29, 95.86, 118.15, 168.96},
            {164.89, 171.20, 100.41, 183.54, 177.75, 122.18, 127.19, 126.86},
            {152.21, 127.71, 85.14, 183.44, 215.04, 111.84, 149.19, 157.14},
            {149.82, 161.01, 143.78, 257.58, 244.48, 182.60, 156.50, 191.12},
            {113.88, 148.27, 101.57, 202.14, 140.71, 105.68, 123.42, 226.74},
            {146.36, 180.06, 149.73, 183.06, 140.68, 161.83, 112.00, 258.63},
            {154.53, 201.77, 216.42, 245.18, 174.84, 124.08, 123.34, 226.98},
            {244.18, 211.87, 231.83, 243.01, 212.73, 174.58, 214.95, 223.17},
            {115.08, 164.50, 209.22, 176.52, 155.65, 99.52, 101.90, 237.71},
            {304.55, 275.30, 310.58, 325.73, 314.02, 214.50, 262.63, 320.12},
            {211.30, 147.44, 183.29, 191.92, 164.24, 102.22, 148.72, 259.10}};
    double[][] aciddata =
            {{4.93, 4.96, 5.06, 5.38, 5.35, 5.08, 5.32, 5.50},
            {0.00, 0.00, 0.00, 0.00, 0.00, 4.78, 5.02, 4.92},
            {4.85, 4.30, 5.56, 6.68, 5.52, 6.15, 6.04, 6.31},
            {5.16, 5.56, 5.95, 6.07, 6.05, 5.83, 6.08, 6.82},
            {5.05, 5.14, 5.48, 5.81, 5.67, 5.59, 5.86, 5.86},
            {5.30, 5.66, 5.54, 5.93, 5.79, 5.78, 6.07, 5.95},
            {5.02, 5.41, 5.25, 5.55, 5.68, 5.25, 5.43, 5.47},
            {5.06, 5.38, 5.81, 6.10, 5.71, 5.88, 5.57, 5.89},
            {5.09, 4.97, 5.12, 4.74, 5.52, 5.09, 5.32, 5.57},
            {5.10, 4.97, 5.41, 5.58, 5.54, 5.39, 5.57, 5.40},
            {0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00},
            {0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00},};
    ImageButton placeButton, yearButton, Kbutton, Hbutton;
    Button acidbutton, rainbutton;
    ConstraintLayout ChartLayout, DataLayout;
    GraphicalView chartView;
    ImageView Taiwan;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);
        findViews();
        yearClickListeners();//宣告年分ImageButton的Listener
        flagClickListeners();//宣告旗子ImageButton的Listener
        chartcreate(1);//地圖建立 設定1為不用清空(一開始建立的沒有東西) 0為清空(原本已經有圖的要清空不然會一直疊上去)
    }
    protected void findViews()
    {
        DataLayout = (ConstraintLayout)findViewById(R.id.DataLayout);
        ChartLayout = (ConstraintLayout)findViewById(R.id.ChartLayout);
        Taiwan = (ImageView)findViewById(R.id.Taiwan);
        Kbutton = (ImageButton)findViewById(R.id.Keelung);
        Hbutton = (ImageButton)findViewById(R.id.Hualien);
        acidbutton = (Button)findViewById(R.id.Acidbutton);
        rainbutton = (Button)findViewById(R.id.Rainbutton);
    }

    public void Acidclick(View v) //連接Acidbutton (直接連xml了)
    {
        DataLayout.setBackground(getResources().getDrawable(R.drawable.shape_acidbackground));//換背景
        Taiwan.setImageResource(R.drawable.ic_aciddatamap);//換台灣顏色的地圖
        datatype = 0;//設定狀態為 Acid data 0
        datanumber = 10; //設定狀態為酸雨資料數 共10筆
        chartcreate(0);
        //10&11是雨量資料有酸雨沒有的資料 再換資料類型的時候重新初始
        locationWanted[10] = 0;
        locationWanted[11] = 0;
        //10 & 11地圖上的棋子換成沒有棋子可以點
        Kbutton.setImageResource(R.drawable.ic_flagnodata);
        Hbutton.setImageResource(R.drawable.ic_flagnodata);
        //把上面兩個切換資料型態的button換成對應的顏色
        acidbutton.setBackgroundColor(Color.rgb(255,114,102));
        rainbutton.setBackgroundColor(Color.rgb(255,255,255));
    }
    public void Rainclick(View v) //連接Rainbutton (直接連xml了)
    {
        DataLayout.setBackground(getResources().getDrawable(R.drawable.shape_rainbackground));//換背景
        Taiwan.setImageResource(R.drawable.ic_raindatamap);//換台灣顏色的地圖
        datatype = 1;//設定狀態為 Rain data 1
        datanumber = 12; //設定狀態為雨量資料數 共12筆
        chartcreate(0);
        //10&11是雨量資料有酸雨沒有的資料 再換資料類型的時候重新初始
        locationWanted[10] = 0;
        locationWanted[11] = 0;
        //10 & 11地圖上的棋子換成只有土的
        Kbutton.setImageResource(R.drawable.ic_mound);
        Hbutton.setImageResource(R.drawable.ic_mound);
        //把上面兩個切換資料型態的button換成對應的顏色
        acidbutton.setBackgroundColor(Color.rgb(255,255,255));
        rainbutton.setBackgroundColor(Color.rgb(68,125,157));
    }

    protected void flagClickListeners()//宣告旗子ImageButton的Listener
    {
        for(int i=0;i<12;i++)
        {
            placeButton = (ImageButton)findViewById(getResources().getIdentifier(location[i], "id", getPackageName()));
            placeButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    ImageButton placeim = (ImageButton)v;
                    changelocationWanted(placeim, placeim.getId(),12,location,locationWanted);
                    chartcreate(0);
                }
            });
        }
    }
    protected void yearClickListeners() //宣告年分ImageButton的Listener
    {
        for(int i=0;i<8;i++)
        {
            yearButton = (ImageButton)findViewById(getResources().getIdentifier("year"+year[i], "id", getPackageName()));
            yearButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    ImageButton yearim = (ImageButton) v;
                    changeyearWanted(yearim, yearim.getId(), 8, year, yearwanted);
                    chartcreate(0);
                }
            });
        }
    }

    protected void changelocationWanted(ImageButton placeim, int clicked, int datanumber, String[] data, int[] datawanted)
    {   //使用者點選的時候直接切換 locationWanted Array的值 ( 0<->1 )
        String clickname = getResources().getResourceEntryName(clicked);
        for(int i=0;i<datanumber;i++)
        {
            if(clickname.equals(data[i])) //先找出被點的是哪一個地點
            {
                if(datatype == 0) //如果是酸雨資料
                {
                    if(i == 10 || i == 11) //10&11(基隆和花蓮) 是沒有資料的所以就算點擊了也不可以顯示 和 更換圖片
                    {
                        datawanted[i] = 0;
                        placeim.setImageResource(getResources().getIdentifier("ic_flagnodata","drawable",getPackageName()));
                    }
                    else
                    {
                        if(datawanted[i] == 0) //其他的資料是正常執行 切換土和棋子與之後的資料要不要跑地圖
                        {
                            datawanted[i] = 1; // 地圖顯示
                            placeim.setImageResource(getResources().getIdentifier("ic_flag"+location[i].toLowerCase(),"drawable",getPackageName()));
                        }
                        else
                        {
                            datawanted[i] = 0; //地圖不顯示
                            placeim.setImageResource(getResources().getIdentifier("ic_mound","drawable",getPackageName()));
                        }
                    }
                }
                else //如果是雨量資料就沒有第10筆和第11筆資料的問題 直接處理就好了
                {
                    if(datawanted[i] == 0)
                    {
                        datawanted[i] = 1;
                        placeim.setImageResource(getResources().getIdentifier("ic_flag"+location[i].toLowerCase(),"drawable",getPackageName()));
                    }
                    else
                    {
                        datawanted[i] = 0;
                        placeim.setImageResource(getResources().getIdentifier("ic_mound","drawable",getPackageName()));
                    }
                }
            }
        }
    }
    protected void changeyearWanted(ImageButton yearim, int clicked, int datanumber, String[] data, int[] datawanted)
    {   //使用者點選的時候直接切換 yearWanted Array的值 ( 0<->1 )
        String clicknamecut = getResources().getResourceEntryName(clicked);
        String clickname = clicknamecut.substring(4,8);
        for(int i=0;i<datanumber;i++)
        {
            if(clickname.equals(data[i]))
            {
                if(datawanted[i] == 0)
                {
                    datawanted[i] = 1;
                    yearim.setImageResource(getResources().getIdentifier("ic_cloudwithrain"+String.valueOf(2009+i),"drawable",getPackageName()));
                }
                else {
                    datawanted[i] = 0;
                    yearim.setImageResource(getResources().getIdentifier("ic_cloud" + String.valueOf(2009 + i), "drawable", getPackageName()));
                }
                break;
            }
        }
    }
    protected void chartcreate(int defaulted)
    {   //地圖建立 設定1為不用清空(一開始建立的沒有東西) 0為清空(原本已經有圖的要清空不然會一直疊上去)
        if(defaulted != 1)
            ((ViewGroup) chartView.getParent()).removeView(chartView);

        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
        for(int i=0;i<datanumber;i++)
        {
            if(locationWanted[i] == 1)
            {
                XYSeries series = new XYSeries(locationName[i]);
                for (int j = 0; j < 8; j++)
                {
                    if(yearwanted[j] == 1)
                    {
                        if(datatype == 0)
                        {
                            if(aciddata[i][j] != 0 )
                                series.add(j + 1, aciddata[i][j]);
                            else
                                series.add(j+1, MathHelper.NULL_VALUE);
                        }
                        else
                        {
                            series.add(j + 1, raindata[i][j]);
                        }
                    }
                }
                dataset.addSeries(series);
            }
        }
        XYMultipleSeriesRenderer renderer = buildRenderer();
        if(datatype ==0)
            setChartSettings(renderer,"酸雨資料","pH",4,7);
        else
            setChartSettings(renderer,"雨量資料","mm", 80, 330);
        chartView = ChartFactory.getLineChartView(this,dataset,renderer);
        ChartLayout.addView(chartView,0);
    }
    private XYMultipleSeriesRenderer buildRenderer()
    {
        XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
        for (int i=0;i < datanumber;i++)
        {
            if(locationWanted[i] == 1)
            {
                XYSeriesRenderer r = new XYSeriesRenderer();
                r.setColor(getLineColor(i));
                r.setLineWidth(3); //線寬度
                r.setPointStyle(PointStyle.CIRCLE);
                r.setPointStrokeWidth(9);
                r.setFillPoints(false);
                renderer.addSeriesRenderer(r);
            }
        }
        return renderer;
    }
    public int getLineColor(int number) //只是用來回傳哪一條線的顏色是哪個旗子的
    {
        switch (number)
        {
            case 0:
                return Color.parseColor("#FF6633");
            case 1:
                return Color.parseColor("#FFAA33");
            case 2:
                return Color.parseColor("#008136");
            case 3:
                return Color.parseColor("#8B6AC0");
            case 4:
                return Color.parseColor("#005594");
            case 5:
                return Color.parseColor("#8180FF");
            case 6:
                return Color.parseColor("#C180FF");
            case 7:
                return Color.parseColor("#FF67D6");
            case 8:
                return Color.parseColor("#FF9167");
            case 9:
                return Color.parseColor("#FF809D");
            case 10:
                return Color.parseColor("#d80024");
            case 11:
                return Color.parseColor("#0099CE");
            default:
                return 0;
        }
    }
    protected void setChartSettings(XYMultipleSeriesRenderer m, String title, String ytitle, double yMin, double yMax)
    {
        m.setChartTitle(title);
        m.setChartTitleTextSize(50);
        m.setXTitle("時間");
        m.setYTitle(ytitle);
        m.setYAxisMin(yMin);//Y軸最大最小值
        m.setYAxisMax(yMax);
        m.setAxesColor(Color.BLACK);//折線圖L型的那個線顏色
        m.setMarginsColor(Color.argb(0x00,0xFF,0xFF,0xFF));//L型的背景顏色
        m.setShowLegend(true);//顯示線的名稱
        m.setLegendTextSize(35);//線的字體大小
        m.setZoomEnabled(false, false);//圖表放大縮小
        m.setLabelsColor(Color.BLACK);//XY軸標題的顏色
        m.setAxisTitleTextSize(35);//XY軸標題字的大小
        m.setXLabelsColor(Color.BLACK);
        for(int i=0;i<8;i++)
        {
            if(yearwanted[i] == 1)
                m.addXTextLabel(i+1, year[i]);
            m.setXLabels(0);
        }
        m.setYLabelsColor(0,Color.BLACK);//Y軸數值的大小
        m.setYLabelsAlign(Paint.Align.RIGHT);//Y軸數值在Y軸的左/右邊
        m.setLabelsTextSize(25);//XY軸數值的大小
        m.setXLabelsPadding(10);
        m.setYLabelsPadding(10);
        m.setMargins(new int[]{100,100,100,100});
        // Disable Pan on two axis
        m.setPanEnabled(false, false);
        m.setShowGrid(true); // we show the grid
    }

    public void ReturnMain(View v) {
        DataActivity.this.finish();
    }

}
